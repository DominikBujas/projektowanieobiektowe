package Exercise1;

public class Triangle extends Polygon {

    PointXY p0, p1, p2;

    public Triangle(PointXY p0, PointXY p1, PointXY p2){
        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;
    }

    public double area(){

        Line l0 = new Line(p0,p1);
        Line l1 = new Line(p1,p2);
        Line l2 = new Line(p0,p2);

        double halfCircum =(l0.getLength() + l1.getLength() + l2.getLength())/2;
        return Math.sqrt(halfCircum * (halfCircum - l0.getLength()) * (halfCircum - l1.getLength()) * (halfCircum - l2.getLength()));
    }
}
