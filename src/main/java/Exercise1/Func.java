package Exercise1;

public class Func extends GeometricObject {

    private FuncObj funcObj;
    private PointXY start;
    private PointXY end;

    public Func(FuncObj funcObj, PointXY start, PointXY end){
        this.funcObj = funcObj;
        this.start = start;
        this.end = end;
    }

    public double getArea(){
        return 1.5;
    }

}
