package Exercise1;

public abstract class Shape extends GeometricObject {

    public abstract double area();
}
