package Exercise1;

public class Circle extends Shape {

    private int radius;
    private PointXY center;

    public Circle(int radius, PointXY center){
        this.radius = radius;
        this.center = center;
    }

    public double area(){
        return 3.14 * Math.pow(this.radius, 2);
    }
}
