package Exercise1;

import java.awt.*;

public class Rectangle extends Polygon {

    PointXY p0, p1, p2, p3;

    public Rectangle(PointXY p0, PointXY p1, PointXY p2, PointXY p3){
        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;

    }

    public double area(){
        Line l1 = new Line(p0,p1);
        Line l2 = new Line(p0,p2);

        return l1.getLength() * l2.getLength();
    }
}
