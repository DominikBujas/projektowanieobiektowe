package Exercise1;

public class Line extends GeometricObject {

    private PointXY start, end;

    public Line(PointXY p1, PointXY p2){
        start = p1;
        end = p2;
    }

    public double getLength(){
        return Math.hypot(
                (start.getX()-end.getX()),(start.getY()-end.getY()));
    }

    public PointXY getStart(){
        return this.start;
    }
    public PointXY getEnd(){
        return this.end;
    }
}
