package Exercise1;

public class PointXY {

    private int x,y;

    public PointXY(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }

}
