package Exercise1;

public abstract class Polygon extends Shape {
    @Override
    public abstract double area();

}
