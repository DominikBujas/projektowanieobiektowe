package Exercise1;

public class ex1 {

    public static void main(String args[]){
        System.out.println("Welcome");

        Triangle t = new Triangle(new PointXY(0,0),new PointXY(0,8),new PointXY(6,0));
        System.out.println("triangle area = " + t.area());

        Rectangle r = new Rectangle(new PointXY(0,0), new PointXY(5,0), new PointXY(0,5),new PointXY(5,5));
        System.out.println("rectangle area = " + r.area());

        Line l = new Line(new PointXY(0,0), new PointXY(3,4));
        System.out.println("line length = " + l.getLength());

        Circle c = new Circle(5, new PointXY(5,5)) ;
        System.out.println("Circle area = " + c.area());

        Func func = new Func(new FuncObj("x"), new PointXY(1,2), new PointXY(3,4));
        System.out.println("Func area = " + c.area());
    }
}
