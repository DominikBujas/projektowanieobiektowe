package Exercise3;

public class ScianaKomorkowa implements Organella {

    private String info;

    public ScianaKomorkowa(String info){
        this.info = info;
    }

    @Override
    public String getInfo() {
        return "Sciana Komorkowa" + this.info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }
}
