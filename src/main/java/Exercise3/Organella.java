package Exercise3;

public interface Organella {

    public String getInfo();
    public void setInfo(String s);
}
