package Exercise3;

import java.util.List;

/**
 * Created by DD on 18/11/02.
 */
public class Plant {
    private List<BlonaKomorkowa> blonaKomorkowa;
    private final ScianaKomorkowa scianaKomorkowa;
    private List<BlonaJadrowa> blonaJadrowa;
    private List<Cytoplazma> cytoplazma;

    Plant(List<BlonaKomorkowa> blonaKomorkowa, ScianaKomorkowa scianaKomorkowa,
          List<BlonaJadrowa> blonaJadrowa, List<Cytoplazma> cytoplazma){
        this.blonaKomorkowa = blonaKomorkowa;
        this.scianaKomorkowa = scianaKomorkowa;
        this.blonaJadrowa = blonaJadrowa;
        this.cytoplazma = cytoplazma;
    }

    public String getBlonaKomorkowa()
    {
        if (blonaKomorkowa.isEmpty()){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(BlonaKomorkowa b : blonaKomorkowa){
            sb.append(b.getInfo()).append("\n");
        }
        return sb.toString();
    }

    public String getBlonaJadrowa() {
        if (blonaJadrowa.isEmpty()){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(BlonaJadrowa b : blonaJadrowa){
            sb.append(b.getInfo()).append("\n");
        }
        return sb.toString();
    }

    public String getCytoplazma()
    {
        if (cytoplazma.isEmpty()){
        return "";
        }
        StringBuilder sb = new StringBuilder();
        for(Cytoplazma c : cytoplazma){
            sb.append(c.getInfo()).append("\n");
        }
        return sb.toString();
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Plant: ").append(scianaKomorkowa.getInfo()).append("\n").append
                (getBlonaKomorkowa()).append(getBlonaJadrowa()).append(getCytoplazma());
        return sb.toString();
    }
}
