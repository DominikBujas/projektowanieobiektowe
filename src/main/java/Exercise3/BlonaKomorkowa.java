package Exercise3;

public class BlonaKomorkowa implements Organella{

    private String info;

    public BlonaKomorkowa(String info){
        this.info = info;
    }

    @Override
    public String getInfo() {
        return "Blona Jadrowa: " + this.info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }
}
