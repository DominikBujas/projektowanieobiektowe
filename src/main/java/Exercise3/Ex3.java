package Exercise3;

public class Ex3 {

    public static void main(String args[]){
        System.out.println("Excercise 3");

        AnimalBuilder rabbitBuilder = new AnimalBuilder(new JadroKomorkowe("rabbit"));
        rabbitBuilder.setBlonaJadrowa(new BlonaJadrowa("rabbit")).setBlonaJadrowa(new BlonaJadrowa("rabbit2"));

        Animal rabbit = rabbitBuilder.createAnimal();
        System.out.println(rabbit);

        System.out.println("===============================\n\n");

        AnimalBuilder flowerBuilder = new AnimalBuilder(new JadroKomorkowe("flower"));
        rabbitBuilder.setBlonaJadrowa(new BlonaJadrowa("flower")).setBlonaJadrowa(new BlonaJadrowa("flower2"))
                .setCytoplazma(new Cytoplazma("flower2")).setBlonaJadrowa(new BlonaJadrowa("flower2"))
                .setCytoplazma(new Cytoplazma("flower2"));

        Animal flower = rabbitBuilder.createAnimal();
        System.out.println(flower);

    }
}
