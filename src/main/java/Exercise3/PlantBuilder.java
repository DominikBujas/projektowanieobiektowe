package Exercise3;

import java.util.ArrayList;
import java.util.List;

public class PlantBuilder {

    private List<BlonaKomorkowa> blonaKomorkowa = new ArrayList<>();
    private final JadroKomorkowe jadroKomorkowe;
    private List<BlonaJadrowa> blonaJadrowa = new ArrayList<>();
    private List<Cytoplazma> cytoplazma = new ArrayList<>();

    public PlantBuilder(JadroKomorkowe j){
        jadroKomorkowe = j;
        blonaKomorkowa.add(new BlonaKomorkowa(""));
        blonaJadrowa.add(new BlonaJadrowa(""));
        cytoplazma.add(new Cytoplazma(""));
    }

//    public JadroKomorkowe getJadroKomorkowe() {
//        return jadroKomorkowe;
//    }

//    public void setJadroKomorkowe(JadroKomorkowe jadroKomorkowe) {
//        this.jadroKomorkowe = jadroKomorkowe;
//    }

    public Animal createAnimal(){
        return new Animal(blonaKomorkowa, jadroKomorkowe, blonaJadrowa, cytoplazma);
    }

    public String getBlonaKomorkowa()
    {
        StringBuilder sb = new StringBuilder();
        for(BlonaKomorkowa b : blonaKomorkowa){
            sb.append(b).append("\n");
        }
        return sb.toString();
    }

    public PlantBuilder setBlonaKomorkowa(BlonaKomorkowa blonaKomorkowa) {
        this.blonaKomorkowa.add(blonaKomorkowa);
        return this;
    }

    public String getBlonaJadrowa() {

        StringBuilder sb = new StringBuilder();
        for(BlonaJadrowa b : blonaJadrowa){
            sb.append(b).append("\n");
        }
        return sb.toString();
    }

    public PlantBuilder setBlonaJadrowa(BlonaJadrowa blonaJadrowa) {
        this.blonaJadrowa.add(blonaJadrowa);
        return this;
    }

    public String getCytoplazma()
    {
        StringBuilder sb = new StringBuilder();
        for(Cytoplazma c : cytoplazma){
            sb.append(c).append("\n");
        }
        return sb.toString();
    }

    public PlantBuilder setCytoplazma(Cytoplazma cytoplazma) {
        this.cytoplazma.add(cytoplazma);
        return this;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Animal: ").append(jadroKomorkowe).append("\n").append
                (getBlonaJadrowa()).append(getBlonaKomorkowa()).append(getCytoplazma());
        return sb.toString();
    }
}
