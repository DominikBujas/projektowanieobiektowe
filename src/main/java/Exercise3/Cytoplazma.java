package Exercise3;

public class Cytoplazma implements Organella {

    private String info;

    public Cytoplazma(String info){
        this.info = info;
    }


    @Override
    public String getInfo() {
        return "Cytoplazma: " + this.info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }
}
