package Exercise3;

public class JadroKomorkowe implements Organella{

    private String info;

    public JadroKomorkowe(String info){
        this.info = info;
    }

    @Override
    public String getInfo() {
        return "JadroKomorkowe; " + this.info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }
}
