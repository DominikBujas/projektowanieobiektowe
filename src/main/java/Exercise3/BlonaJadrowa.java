package Exercise3;

public class BlonaJadrowa implements Organella{

    private String info;

    public BlonaJadrowa(String info){
        this.info = info;
    }

    @Override
    public String getInfo() {
        return "Blona komorkowa: " + this.info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }
}
