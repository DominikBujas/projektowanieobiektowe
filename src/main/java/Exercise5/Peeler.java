package Exercise5;

public class Peeler implements ElectronicProduct {

    public Peeler(){
        comps.add(new C());
        comps.add(new D());
        comps.add(new A());
        comps.add(new D());
    }


    @Override
    public ElectronicProduct clone(){
        try {
            return (Peeler) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void create(){
        comps.add(new C());
        comps.add(new D());
        comps.add(new A());
        comps.add(new D());
    }
}
