package Exercise5;

public class Factory {
    Peeler p;
    Retro r;
    Toster t;

    public Factory (){
        p = new Peeler();
        r = new Retro();
        t = new Toster();
    }

    public ElectronicProduct elFactory(String name){
        switch(name){
            case "Peeler":
                return p.clone();
            case "Retro":
                return r.clone();
            case "Toster":
                return t.clone();
        }
        return null;
    }

}
