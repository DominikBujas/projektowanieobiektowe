package Exercise5;

public class Ex5 {
    public static void main(String args[]){
        System.out.println("-----------Start-----------");

        Factory f = new Factory();

        Peeler p1 = (Peeler)f.elFactory("Peeler");
        Peeler p2 = (Peeler)f.elFactory("Peeler");
        Retro r1 = (Retro)f.elFactory("Retro");
        Toster t1 = (Toster)f.elFactory("Toster");
        Retro r2 = (Retro)f.elFactory("Retro");
        Peeler p3 = (Peeler)f.elFactory("Peeler");

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(r1);
        System.out.println(t1);
        System.out.println(r2);
        System.out.println(p3);

        System.out.println("------------End------------");
    }
}
