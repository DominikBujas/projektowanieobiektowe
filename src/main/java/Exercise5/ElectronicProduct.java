package Exercise5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface ElectronicProduct extends Cloneable {

    public ElectronicProduct clone() throws CloneNotSupportedException;
    public void create();
    List<Component> comps = new ArrayList<>();
}
