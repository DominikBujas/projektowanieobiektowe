package Exercise5;

public class Toster implements ElectronicProduct {

    public Toster(){
        this.create();
    }

    @Override
    public ElectronicProduct clone(){
        try {
            return (Toster) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void create(){
        comps.add(new A());
        comps.add(new B());
        comps.add(new B());
        comps.add(new A());
        comps.add(new D());
    }
}
