package Exercise5;

public class Retro implements ElectronicProduct {

    public Retro(){
        this.create();
    }

    @Override
    public ElectronicProduct clone(){
        try {
            return (Retro) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void create(){
        comps.add(new A());
        comps.add(new A());
        comps.add(new A());
        comps.add(new B());
        comps.add(new B());
        comps.add(new C());
    }
}
