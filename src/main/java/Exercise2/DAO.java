package Exercise2;

import com.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class DAO {

    private static DAO instance;
    private static String path = "src/main/java/Exercise2/";
    private static String BOOKS_FILE = path + "LibraryBooks.csv";
    private static String USERS_FILE = path + "LibraryUsers.csv";
    private Books books;
    private Users users;

    public DAO() {
    }

    public static synchronized DAO getInstance() {
        if (instance == null) {
            instance = new DAO();
        }
        return instance;
    }

    public void createCsvObjects(){
        books = new Books(BOOKS_FILE);
        users = new Users(USERS_FILE);
    }

    public Books getBooks(){
        return this.books;
    }

    public Users getUsers(){
        return this.users;
    }

}
