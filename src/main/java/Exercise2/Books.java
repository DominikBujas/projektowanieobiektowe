package Exercise2;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Books {

    String csvFile;
    String line = "";
    char separator = ';';

    Books(String path){
        csvFile = path;

    }

    public void addBook(String Id, String title, String author, String userId) throws java.io.IOException{

        StringBuilder sb = new StringBuilder();
        sb.append("\n").append(Id).append(";").append(title).append(";").append(author).append(";").append(userId);
//
//        CSVReader reader2 = new CSVReader(new FileReader(csvFile));
//        List<String[]> allElements = reader2.readAll();
//        allElements.add(new String[]{Id,title,author,userId});
//        StringWriter sw = new StringWriter();
//        CSVWriter writer = new CSVWriter(new FileWriter(csvFile, true));
//        writer.writeAll(allElements);

        try {
            Files.write(Paths.get(csvFile), sb.toString().getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String modifyBook(String Id, String title, String author, String userId){
        StringBuilder sb = new StringBuilder();
        sb.append(Id).append(";").append(title).append(";").append(author).append(";").append(userId);
        return sb.toString();
    }

    public void getBooks(){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] book = line.split(";");
                System.out.println("[" + book[0] + "] " + book[1] + " " + book[2] + " " + book[3] );
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void rentBook(int Id, String userId) throws java.io.FileNotFoundException, java.io.IOException{
        File inputFile = new File(csvFile);

        CSVReader reader = new CSVReader(new FileReader(inputFile), separator);
        List<String[]> csvBody = reader.readAll();
        csvBody.get(Id)[3] = userId;
        reader.close();

        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), separator);
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
    }
}
