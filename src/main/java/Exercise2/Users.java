package Exercise2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Users {

    String csvFile;
    String line = "";
    String cvsSplitBy = ";";

    public Users(String path){

        csvFile = path;
    }

    public void addUser(String Id, String name, String surname, String year, String penalty){
        StringBuilder sb = new StringBuilder();
        sb.append(Id).append(";").append(name).append(";").append(surname).append(";").append(year).append(";").append(penalty);
        try {
            Files.write(Paths.get(csvFile), sb.toString().getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String modifyUser(String Id, String name, String surname, String year, String penalty){
        StringBuilder sb = new StringBuilder();
        sb.append(Id).append(";").append(name).append(";").append(surname).append(";").append(year).append(";").append(penalty);
        return sb.toString();
    }

    public void getUsers(){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] user = line.split(cvsSplitBy);
                System.out.println("[" + user[0] + "] " + user[1] + " " + user[2] + " " + user[3] + " " + user[4]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
