package Excercise4;

public class XmlSerializer implements Serializer {

    String information;

    @Override
    public void serializeInformation(Information info){
        StringBuilder sb = new StringBuilder();
        sb.append("<information>\n\t");
        sb.append("<name>").append(info.getName()).append("</name>\n\t");
        sb.append("<number>").append(info.getNumber()).append("</number>\n\t");
        sb.append("<games>\n\t\t");
        for(String game : info.getGames()){
            sb.append("<game>").append(game).append("</game>\n\t\t");
        }
        sb.append("\b</games>\n");
        sb.append("</information>");

        information = sb.toString();
    }

    @Override
    public String getInformation() {
        return information;
    }
}
