package Excercise4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlDeserializer implements Deserializer {

    @Override
    public Information getData(String info) {

        String name = info.substring(info.indexOf("<name>") + 6,info.indexOf("</name>"));;
        int number = Integer.parseInt(info.substring(info.indexOf("<number>") + 8,info.indexOf("</number>")));

        Information information = new Information(number,name);

        String regexString = Pattern.quote("<game>") + "(.*?)" + Pattern.quote("</game>");
        Pattern pattern = Pattern.compile(regexString);
        Matcher matcher = pattern.matcher(info);

        while (matcher.find()) {
            String game = matcher.group(1);
            information.addGame(game);
        }
        return information;
    }
}
