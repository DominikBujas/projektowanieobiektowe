package Excercise4;

public class CsvDeserializer implements Deserializer {


    @Override
    public Information getData(String info) {

        String data[] = info.split(";");
        Information information = new Information( Integer.parseInt(data[1]), data[0]);

        for(int i = 2; i < data.length; i++){
            information.addGame(data[i]);
        }

        return information;
    }
}
