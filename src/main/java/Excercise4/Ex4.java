package Excercise4;

public class Ex4 {

    public static void main(String args[]){

        Information info = new Information(1, "games");
        info.addGame("Poker");
        info.addGame("Tetris");
        info.addGame("Blackjack");

        System.out.println("------------Csv serializer------------");
        Serializer csv = new CsvSerializer();
        csv.serializeInformation(info);
        System.out.println(csv.getInformation());

        System.out.println("------------Xml serializer------------");
        Serializer xml = new XmlSerializer();
        xml.serializeInformation(info);
        System.out.println(xml.getInformation());

        System.out.println("------------Csv deserializer------------");
        Deserializer desCsv = new CsvDeserializer();
        System.out.println(desCsv.getData(csv.getInformation()).getInformation());

        System.out.println("------------Xml serializer------------");
        Deserializer desXml = new XmlDeserializer();
        System.out.println(desXml.getData(xml.getInformation()).getInformation());

    }
}
