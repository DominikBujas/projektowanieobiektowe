package Excercise4;

public interface Serializer {
    public void serializeInformation(Information info);
    public String getInformation();
}
