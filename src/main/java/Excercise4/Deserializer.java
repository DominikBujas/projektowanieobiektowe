package Excercise4;

public interface Deserializer {
    public Information getData(String s);
}
