package Excercise4;

import java.util.ArrayList;
import java.util.List;

public class Information {
    private int number;
    private String name;
    private List<String> games;

    public Information(int number, String name){
        this.name = name;
        this.number = number;
        this.games = new ArrayList<>();
    }

    public void addGame(String game){
        this.games.add(game);
    }

    public String getInformation(){
        return "Name = " + name + "\nNumber = " + number + "\nGames: " + games.size();
    }

    public String getName(){ return name;}
    public int getNumber(){ return number;}
    public List<String> getGames(){ return games;}


}
