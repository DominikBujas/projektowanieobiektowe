package Excercise4;

public class CsvSerializer implements Serializer{

    String information;

    @Override
    public void serializeInformation(Information info) {
        StringBuilder sb = new StringBuilder();
        sb.append(info.getName()).append(";").append(info.getNumber()).append(";");
        for(String game : info.getGames()){
            sb.append(game).append(";");
        }
//        sb.append("\b]");
        information = sb.toString();
    }

    @Override
    public String getInformation(){
        return information;
    }
}
